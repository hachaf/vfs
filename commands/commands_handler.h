#ifndef PSEUDONTFS_COMMANDS_HANDLER_H
#define PSEUDONTFS_COMMANDS_HANDLER_H

#include "../runtime/runtime.h"
#include "commands.h"

/**
 * Handle command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle format command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_format_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle incp command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_incp_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle pwd command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_pwd_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Recursively prints current path
 * @param mft_dir_index current directory mft index
 * @param runtime runtime instance
 */
void print_dir_recursive(int32_t mft_dir_index, struct runtime_t * runtime);

/**
 * Handle exit command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_exit_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle mkdir command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_mkdir_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle ls command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_ls_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle rmdir command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_rmdir_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle cd command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_cd_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle outcp command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_outcp_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle cat command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_cat_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle load command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_load_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle cp command
 * @param command parsed command
 * @param runtime runtime instance
 */
int handle_cp_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle rm command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_rm_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle mv command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_mv_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle info command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_info_command(struct command_t * command, struct runtime_t * runtime);

/**
 * Handle defrag command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_defrag_command(struct runtime_t * runtime);

#endif //PSEUDONTFS_COMMANDS_HANDLER_H

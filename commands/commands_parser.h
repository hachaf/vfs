#ifndef PSEUDONTFS_COMMANDS_PARSER_H
#define PSEUDONTFS_COMMANDS_PARSER_H

#include "commands.h"

/**
 * Read command from stdin
 * @return parsed command
 */
struct command_t * read_command();

/**
 * Parse command
 * @param line line string
 * @return parsed command
 */
struct command_t * parse_command(char * line);

#endif //PSEUDONTFS_COMMANDS_PARSER_H

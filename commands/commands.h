#ifndef PSEUDONTFS_COMMANDS_H
#define PSEUDONTFS_COMMANDS_H

#define COMMAND_TYPE_FORMAT 0
#define COMMAND_NAME_FORMAT "format"
#define COMMAND_ARGC_FORMAT 1

#define COMMAND_TYPE_INCP 1
#define COMMAND_NAME_INCP "incp"
#define COMMAND_ARGC_INCP 2

#define COMMAND_TYPE_PWD 2
#define COMMAND_NAME_PWD "pwd"
#define COMMAND_ARGC_PWD 0

#define COMMAND_TYPE_MKDIR 3
#define COMMAND_NAME_MKDIR "mkdir"
#define COMMAND_ARGC_MKDIR 1

#define COMMAND_TYPE_LS 4
#define COMMAND_NAME_LS "ls"
#define COMMAND_ARGC_LS 0

#define COMMAND_TYPE_RMDIR 5
#define COMMAND_NAME_RMDIR "rmdir"
#define COMMAND_ARGC_RMDIR 1

#define COMMAND_TYPE_CD 6
#define COMMAND_NAME_CD "cd"
#define COMMAND_ARGC_CD 1

#define COMMAND_TYPE_OUTCP 7
#define COMMAND_NAME_OUTCP "outcp"
#define COMMAND_ARGC_OUTCP 2

#define COMMAND_TYPE_CAT 8
#define COMMAND_NAME_CAT "cat"
#define COMMAND_ARGC_CAT 1

#define COMMAND_TYPE_LOAD 9
#define COMMAND_NAME_LOAD "load"
#define COMMAND_ARGC_LOAD 1

#define COMMAND_TYPE_CP 10
#define COMMAND_NAME_CP "cp"
#define COMMAND_ARGC_CP 2

#define COMMAND_TYPE_MV 11
#define COMMAND_NAME_MV "mv"
#define COMMAND_ARGC_MV 2

#define COMMAND_TYPE_RM 12
#define COMMAND_NAME_RM "rm"
#define COMMAND_ARGC_RM 1

#define COMMAND_TYPE_INFO 13
#define COMMAND_NAME_INFO "info"
#define COMMAND_ARGC_INFO 1

#define COMMAND_TYPE_DEFRAG 14
#define COMMAND_NAME_DEFRAG "defrag"
#define COMMAND_ARGC_DEFRAG 0

#define COMMAND_TYPE_EXIT 99
#define COMMAND_NAME_EXIT "exit"
#define COMMAND_ARGC_EXIT 0

/**
 * Command structure
 */
struct command_t {
    int type;
    char ** args;
    int argc;
};

#endif //PSEUDONTFS_COMMANDS_H

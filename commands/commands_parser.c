#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "commands_parser.h"
#include "../logger/logger.h"

char * _read_command_line();


/**
 * Read command from stdin
 * @return parsed command
 */
struct command_t * read_command() {
    fputc('>', stdout);
    fputc(' ', stdout);

    char * line = _read_command_line();
	
	return parse_command(line);
}

/**
 * Parse command
 * @param line line string
 * @return parsed command
 */
struct command_t * parse_command(char * line) {
    int i;
    struct command_t * command = NULL;
    char * token;
    token = strtok(line," ");

    if (!token) {
        return NULL;
    }

    command = malloc(sizeof(struct command_t));
    if (!command) {
        log_error("Cannot allocate memory");
        return NULL;
    }

    if (!strcmp(token, COMMAND_NAME_FORMAT)) {
        command->type = COMMAND_TYPE_FORMAT;
        command->argc = COMMAND_ARGC_FORMAT;
    } else if (!strcmp(token, COMMAND_NAME_INCP)) {
        command->type = COMMAND_TYPE_INCP;
        command->argc = COMMAND_ARGC_INCP;
    } else if (!strcmp(token, COMMAND_NAME_PWD)) {
        command->type = COMMAND_TYPE_PWD;
        command->argc = COMMAND_ARGC_PWD;
    } else if (!strcmp(token, COMMAND_NAME_MKDIR)) {
        command->type = COMMAND_TYPE_MKDIR;
        command->argc = COMMAND_ARGC_MKDIR;
    } else if (!strcmp(token, COMMAND_NAME_LS)) {
        command->type = COMMAND_TYPE_LS;
        command->argc = COMMAND_ARGC_LS;
    } else if (!strcmp(token, COMMAND_NAME_RMDIR)) {
        command->type = COMMAND_TYPE_RMDIR;
        command->argc = COMMAND_ARGC_RMDIR;
    } else if (!strcmp(token, COMMAND_NAME_CD)) {
        command->type = COMMAND_TYPE_CD;
        command->argc = COMMAND_ARGC_CD;
    } else if (!strcmp(token, COMMAND_NAME_OUTCP)) {
        command->type = COMMAND_TYPE_OUTCP;
        command->argc = COMMAND_ARGC_OUTCP;
    } else if (!strcmp(token, COMMAND_NAME_CAT)) {
        command->type = COMMAND_TYPE_CAT;
        command->argc = COMMAND_ARGC_CAT;
    } else if (!strcmp(token, COMMAND_NAME_LOAD)) {
        command->type = COMMAND_TYPE_LOAD;
        command->argc = COMMAND_ARGC_LOAD;
    } else if (!strcmp(token, COMMAND_NAME_EXIT)) {
        command->type = COMMAND_TYPE_EXIT;
        command->argc = COMMAND_ARGC_EXIT;
    } else if (!strcmp(token, COMMAND_NAME_CP)) {
        command->type = COMMAND_TYPE_CP;
        command->argc = COMMAND_ARGC_CP;
    } else if (!strcmp(token, COMMAND_NAME_RM)) {
        command->type = COMMAND_TYPE_RM;
        command->argc = COMMAND_ARGC_RM;
    } else if (!strcmp(token, COMMAND_NAME_MV)) {
        command->type = COMMAND_TYPE_MV;
        command->argc = COMMAND_ARGC_MV;
    } else if (!strcmp(token, COMMAND_NAME_INFO)) {
        command->type = COMMAND_TYPE_INFO;
        command->argc = COMMAND_ARGC_INFO;
    } else if (!strcmp(token, COMMAND_NAME_DEFRAG)) {
        command->type = COMMAND_TYPE_DEFRAG;
        command->argc = COMMAND_ARGC_DEFRAG;
    } else {
        log_info("Unknown command.");
        free(command);
        return NULL;
    }

    command->args = malloc(command->argc * sizeof(char *));
    if (!command->args) {
        log_error("Command args cannot be allocated.");
        free(command);
        return NULL;
    }

    for (i = 0; i < command->argc; i++) {
        command->args[i] = strtok (NULL, " ");
    }

    return command;
}

/**
 * Read line from stdin
 * @return line string
 */
char * _read_command_line() {
    char * line = malloc(100), * linep = line;
    size_t lenmax = 100, len = lenmax;
    int c;

    if(line == NULL)
        return NULL;

    for(;;) {
        c = fgetc(stdin);
        if(c == EOF)
            break;

        if(--len == 0) {
            len = lenmax;
            char * linen = realloc(linep, lenmax *= 2);

            if(linen == NULL) {
                free(linep);
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }

        if((char) tolower(c) == '\n')
            break;

        *line++ = (char) tolower(c);
    }
    *line = '\0';
    return linep;
}

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "commands_handler.h"
#include "../runtime/runtime.h"
#include "commands.h"
#include "../logger/logger.h"
#include "../structures/boot_record.h"
#include "../structures/mft_fragment.h"
#include "../structures/mft_item.h"
#include "../structures/directory_descriptor.h"
#include "../vfs/vfs_functions.h"
#include "commands_parser.h"

/**
 * Handle command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_command(struct command_t * command, struct runtime_t * runtime) {
    if (!runtime->filesystem_exists && command->type != COMMAND_TYPE_FORMAT) {
        printf("FILESYSTEM DOES NOT EXIST\n");
        return;
    }

    switch (command->type) {
        case COMMAND_TYPE_FORMAT:
            handle_format_command(command, runtime);
            break;
        case COMMAND_TYPE_INCP:
            handle_incp_command(command, runtime);
            break;
        case COMMAND_TYPE_PWD:
            handle_pwd_command(command, runtime);
            break;
        case COMMAND_TYPE_MKDIR:
            handle_mkdir_command(command, runtime);
            break;
        case COMMAND_TYPE_LS:
            handle_ls_command(command, runtime);
            break;
        case COMMAND_TYPE_RMDIR:
            handle_rmdir_command(command, runtime);
            break;
        case COMMAND_TYPE_CD:
            handle_cd_command(command, runtime);
            break;
        case COMMAND_TYPE_EXIT:
            handle_exit_command(command, runtime);
            break;
        case COMMAND_TYPE_OUTCP:
            handle_outcp_command(command, runtime);
            break;
        case COMMAND_TYPE_CAT:
            handle_cat_command(command, runtime);
            break;
        case COMMAND_TYPE_LOAD:
            handle_load_command(command, runtime);
            break;
        case COMMAND_TYPE_CP:
            handle_cp_command(command, runtime);
            break;
        case COMMAND_TYPE_RM:
            handle_rm_command(command, runtime);
            break;
        case COMMAND_TYPE_MV:
            handle_mv_command(command, runtime);
            break;
        case COMMAND_TYPE_INFO:
            handle_info_command(command, runtime);
            break;
        case COMMAND_TYPE_DEFRAG:
            handle_defrag_command(runtime);
            break;
        default: //should not be entered
            break;
    }
}

/**
 * Handle format command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_format_command(struct command_t * command, struct runtime_t * runtime) {
    int i, fs_size;
    fs_size = atoi(command->args[0]); // NOLINT(cert-err34-c)
    if (!fs_size) {
        log_error("Invalid filesystem size.\n");
        return;
    }

    fs_size *= 1024 * 1024;

    FILE * f = fopen(runtime->filepath, "r");
    if (f) {
        printf("CANNOT CREATE FILE.\n");
        fclose(f);
        remove(runtime->filepath);
    }

    int avsize = fs_size - sizeof(struct boot_record);
    int n = avsize / ((sizeof(struct mft_item)/10) + sizeof(int32_t) + DATA_CLUSTER_SIZE);

    runtime->br = malloc(sizeof(struct boot_record));
    runtime->br->disk_size = fs_size;
    runtime->br->mft_start_address = sizeof(struct boot_record);
    runtime->br->mft_max_fragment_count = n / 10;
    runtime->br->bitmap_start_address = runtime->br->mft_start_address + (sizeof(struct mft_item) * runtime->br->mft_max_fragment_count);
    runtime->br->data_start_address = runtime->br->bitmap_start_address + (sizeof(int32_t) * n);
    runtime->br->cluster_count = n;

    if (!runtime->file) {
        runtime->file = fopen(runtime->filepath, "w+");
    }
    fseek(runtime->file, 0, SEEK_SET);
    fwrite(runtime->br, sizeof(struct boot_record), 1, runtime->file);

    runtime->mfts = malloc(sizeof(struct mft_item *) * runtime->br->mft_max_fragment_count);
    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        runtime->mfts[i] = malloc(sizeof(struct mft_item));
        runtime->mfts[i]->uid = UID_ITEM_FREE;
        fwrite(runtime->mfts[i], sizeof(struct mft_item), 1, runtime->file);
    }

    runtime->bitmap = malloc(sizeof(int32_t) * runtime->br->cluster_count);
    for (i = 0; i < runtime->br->cluster_count; i++) {
        runtime->bitmap[i] = 0;
        fwrite(&(runtime->bitmap[i]), sizeof(int32_t), 1, runtime->file);
    }

    char empty_cluster = 0;
    for (i = 0; i < n * DATA_CLUSTER_SIZE; i++) {
        fwrite(&empty_cluster, sizeof(char), 1, runtime->file);
    }

    fseek(runtime->file, 0, SEEK_SET);

    //write root directory
    struct directory_descriptor * dir = malloc(sizeof(struct directory_descriptor));
    dir->parent_mft = -1;
    dir->is_root = 1;
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        dir->files_mft[i] = DIRECTORY_FILE_FREE;
    }
    runtime->pwd_mft_index = 0;

    fseek(runtime->file, sizeof(struct boot_record) + (sizeof(struct mft_item) * runtime->br->mft_max_fragment_count) + (sizeof(int32_t) * runtime->br->cluster_count), SEEK_SET);
    fwrite(dir, sizeof(struct directory_descriptor), 1, runtime->file);

    runtime->mfts[0]->is_directory = 1;
    runtime->mfts[0]->item_name[0] = '/';
    runtime->mfts[0]->item_name[1] = '\0';
    runtime->mfts[0]->uid = 0;
    runtime->mfts[0]->item_order = 1;
    runtime->mfts[0]->item_order_total = 1;
    runtime->mfts[0]->item_size = sizeof(struct directory_descriptor);
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCDFAInspection"
    runtime->mfts[0]->fragments[0].fragment_count = sizeof(struct directory_descriptor) / DATA_CLUSTER_SIZE + (sizeof(struct directory_descriptor) % DATA_CLUSTER_SIZE > 0 ? 1 : 0);
#pragma clang diagnostic pop
    runtime->mfts[0]->fragments[0].fragment_start_address = 0;

    runtime->bitmap[0] = 1;

    persist_vfs(runtime);
    runtime->filesystem_exists = 1;
    printf("OK\n");
}

/**
 * Handle incp
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_incp_command(struct command_t * command, struct runtime_t * runtime) {
    int i, j, free_clusters_count, mft_fragment_index, mft_index, addr;
    size_t content_size;
    long file_size, data_remains;
    struct mft_item * mft = NULL;
    void * data_buffer;
    struct directory_descriptor * parent;
    mft_index = 0;

    parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);
    
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] != DIRECTORY_FILE_FREE) {
            if (!strcmp(runtime->mfts[parent->files_mft[i]]->item_name, command->args[1])) {
                printf("File or directory with this name already exists.");
                free(parent);
                return;
            }
        }
    }
    
    FILE * file = fopen(command->args[0], "r");
    if (!file) {
        printf("FILE NOT FOUND (missing source)\n");
	return;
    }

    //get file size
    fseek(file, 0L, SEEK_END);
    data_remains = file_size = ftell(file);
    fseek(file, 0L, SEEK_SET);
    
    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        if (runtime->mfts[i]->uid == UID_ITEM_FREE) {
            mft_index = i;
            mft = runtime->mfts[i];
            mft->uid = i;
            mft->is_directory = 0;
            mft->item_size = (int32_t) file_size;
            mft->item_order_total = 0;
            mft->item_order = 1;
            break;
        }
    }

    if (!mft) {
        printf("Free MFT not found.\n");
        return;
    }

    for (i = 0; i < FILENAME_MAX; i++) {
        mft->item_name[i] = command->args[1][i];
        if (mft->item_name[i] == '\0') {
            break;
        }
    }
    
    mft_fragment_index = 0;
    while (data_remains > 0) {
        mft->item_order_total++;
        for (i = 0; i < runtime->br->cluster_count; i++) {
            if (!runtime->bitmap[i]) {
				addr = i;
                free_clusters_count = 1;
                while (!runtime->bitmap[++i]) {
                    free_clusters_count++;
                }
                if (free_clusters_count > ((data_remains / DATA_CLUSTER_SIZE) + (data_remains % DATA_CLUSTER_SIZE > 0) ? 1 : 0)) {
                    free_clusters_count = ((data_remains / DATA_CLUSTER_SIZE) + (data_remains % DATA_CLUSTER_SIZE > 0) ? 1 : 0);
                }
                for (j = 0; j < free_clusters_count; j++) {
                    runtime->bitmap[addr + j] = 1;
                }
                mft->fragments[mft_fragment_index].fragment_count = free_clusters_count;
                mft->fragments[mft_fragment_index].fragment_start_address = addr;
				fprintf(stderr, "ad %d\n", i);

                fseek(runtime->file, runtime->data_start, SEEK_SET);
                fseek(runtime->file, addr * DATA_CLUSTER_SIZE, SEEK_CUR);

                content_size = free_clusters_count * DATA_CLUSTER_SIZE > data_remains ? (size_t) data_remains : free_clusters_count * DATA_CLUSTER_SIZE;

                data_buffer = malloc(content_size);
                fread(data_buffer, content_size, 1, file);
                fwrite(data_buffer, content_size, 1, runtime->file);
                free(data_buffer);
                data_remains -= free_clusters_count * DATA_CLUSTER_SIZE > data_remains ? data_remains : free_clusters_count * DATA_CLUSTER_SIZE;

                break;
            }
        }

        mft_fragment_index++;
    }

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] == DIRECTORY_FILE_FREE) {
            parent->files_mft[i] = mft_index;
            break;
        }
    }

    fseek(runtime->file, runtime->data_start, SEEK_SET);
    fseek(runtime->file, runtime->mfts[runtime->pwd_mft_index]->fragments[0].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
    fwrite(parent, sizeof(struct directory_descriptor), 1, runtime->file);

    persist_vfs(runtime);
    free(parent);

    printf("OK\n");
}

/**
 * Handle pwd command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_pwd_command(struct command_t * command, struct runtime_t * runtime) {
    print_dir_recursive(runtime->pwd_mft_index, runtime);
    printf("\n");
}

/**
 * Recursively prints current path
 * @param mft_dir_index current directory mft index
 * @param runtime runtime instance
 */
void print_dir_recursive(int32_t mft_dir_index, struct runtime_t * runtime) {
    struct directory_descriptor * dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, runtime->mfts[mft_dir_index], runtime);

    if (!dir->is_root) {
        print_dir_recursive(dir->parent_mft, runtime);
    }

    printf("%s", runtime->mfts[mft_dir_index]->item_name);
    printf("/");

    free(dir);
}

/**
 * Handle exit command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_exit_command(struct command_t * command, struct runtime_t * runtime) {
    runtime->exit = 1;
    fclose(runtime->file);
}

/**
 * Handle mkdir command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_mkdir_command(struct command_t * command, struct runtime_t * runtime) {
    int i, mft_index, j, token_index, dir_found;
    char * token, ** tokens;
    struct mft_item * mft, * current_mft;
    struct directory_descriptor * dir;

    //prectu source sekvenci umisteni
    token_index = 0;
    token = strtok(command->args[0], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        dir_found = 0;
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                mft_index = dir->files_mft[j];
                dir_found = 1;
                break;
            }
        }
        free(dir);
        if (!dir_found) {
            printf("PATH NOT FOUND\n");
            return;
        }
    }


    struct directory_descriptor * parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, current_mft, runtime);
    for (i = 0 ; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] == DIRECTORY_FILE_FREE) {
            continue;
        }
        if (!strcmp(command->args[0], runtime->mfts[parent->files_mft[i]]->item_name)) {
            printf("EXIST\n");
            free(parent);
            return;
        }
    }

    for (i = 0 ; i < runtime->br->mft_max_fragment_count; i++) {
        if (runtime->mfts[i]->uid == UID_ITEM_FREE) {
            runtime->mfts[i]->uid = i;
            runtime->mfts[i]->is_directory = 1;
            runtime->mfts[i]->item_order_total = 1;
            runtime->mfts[i]->item_order = 1;
            runtime->mfts[i]->item_size = sizeof(struct directory_descriptor);
            for (j = 0; j < FILENAME_MAX; j++) {
                runtime->mfts[i]->item_name[j] = tokens[token_index-1][j];
                if (command->args[0][j] == '\0') {
                    break;
                }
            }
            mft = runtime->mfts[i];
            mft_index = i;
            break;
        }
    }

    dir = malloc(sizeof(struct directory_descriptor));
    dir->parent_mft = current_mft->uid;
    dir->is_root = 0;
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        dir->files_mft[i] = DIRECTORY_FILE_FREE;
    }

    for (i = 0; i < runtime->br->cluster_count; i++) {
        if (!runtime->bitmap[i]) {
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, DATA_CLUSTER_SIZE * i, SEEK_CUR);
            fwrite(dir, sizeof(struct directory_descriptor), 1, runtime->file);
            runtime->bitmap[i] = 1;
            mft->fragments[0].fragment_count = 1;
            mft->fragments[0].fragment_start_address = i;
            break;
        }
    }

    for (i = 0 ; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] == DIRECTORY_FILE_FREE) {
            parent->files_mft[i] = mft_index;
            break;
        }
    }

    fseek(runtime->file, runtime->data_start, SEEK_SET);
    fseek(runtime->file, DATA_CLUSTER_SIZE * runtime->mfts[dir->parent_mft]->fragments[0].fragment_start_address, SEEK_CUR);
    fwrite(parent, sizeof(struct directory_descriptor), 1, runtime->file);

    persist_vfs(runtime);

    free(dir);
    free(parent);
    printf("OK\n");
}

/**
 * Handle ls command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_ls_command(struct command_t * command, struct runtime_t * runtime) {
    int i;
    struct directory_descriptor * parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);
    for (i = 0 ; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] == DIRECTORY_FILE_FREE) {
            continue;
        }
        printf(runtime->mfts[parent->files_mft[i]]->is_directory ? "+" : "-");
        printf("%s\n", runtime->mfts[parent->files_mft[i]]->item_name);
    }
}

/**
 * Handle rmdir command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_rmdir_command(struct command_t * command, struct runtime_t * runtime) {
    int i, j, mft_index, token_index, dir_found;
    char * token, ** tokens;
    struct mft_item * mft = NULL;
    struct mft_item * current_mft;
    struct directory_descriptor * parent = NULL;
    struct directory_descriptor * dir = NULL;

    //prectu source sekvenci umisteni
    token_index = 0;
    token = strtok(command->args[0], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        dir_found = 0;
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                mft_index = dir->files_mft[j];
                dir_found = 1;
                break;
            }
        }
        free(dir);
        if (!dir_found) {
            printf("FILE NOT FOUND\n");
            return;
        }
    }

    //aktualni adresar
    parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, current_mft, runtime);

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[parent->files_mft[i]]->item_name, tokens[token_index - 1]) && runtime->mfts[parent->files_mft[i]]->is_directory) {
            mft = runtime->mfts[parent->files_mft[i]];
            mft_index = parent->files_mft[i];
        }
    }

    if (!mft) {
        printf("FILE NOT FOUND\n");
        free(parent);
        return;
    }

    //dir to remove
    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, mft, runtime);

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (dir->files_mft[i] != DIRECTORY_FILE_FREE) {
            printf("NOT EMPTY\n");
            free(parent);
            free(dir);
            return;
        }
    }

    runtime->bitmap[mft->fragments[0].fragment_start_address] = 0;
    mft->uid = UID_ITEM_FREE;

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] == mft_index) {
            parent->files_mft[i] = DIRECTORY_FILE_FREE;
            break;
        }
    }

    fseek(runtime->file, runtime->data_start, SEEK_SET);
    fseek(runtime->file, runtime->mfts[current_mft->uid]->fragments[0].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
    fwrite(parent, sizeof(struct directory_descriptor), 1, runtime->file);

    persist_vfs(runtime);

    free(parent);
    free(dir);
    printf("OK\n");
}

/**
 * Handle cd command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_cd_command(struct command_t * command, struct runtime_t * runtime) {
    int i;
    struct directory_descriptor * parent;

    if (!strcmp(command->args[0], "..")) {
        parent = malloc(sizeof(struct directory_descriptor));
        read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);
        if (!parent->is_root) {
            runtime->pwd_mft_index = parent->parent_mft;
        }
        free(parent);
        return;
    }

    if (!strcmp(command->args[0], ".")) {
        return;
    }

    parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] != DIRECTORY_FILE_FREE) {
            if (runtime->mfts[parent->files_mft[i]]->is_directory && !strcmp(runtime->mfts[parent->files_mft[i]]->item_name, command->args[0])) {
                runtime->pwd_mft_index = parent->files_mft[i];
                free(parent);
                return;
            }
        }
    }

    printf("PATH NOT FOUND\n");
    free(parent);
}

/**
 * Handle outcp command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_outcp_command(struct command_t * command, struct runtime_t * runtime) {
    int i;
    struct directory_descriptor * parent;
    struct mft_item * mft = NULL;
    void * content;
    FILE * dest_file;

    parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[parent->files_mft[i]]->item_name, command->args[0])) {
            mft = runtime->mfts[parent->files_mft[i]];
            break;
        }
    }

    if (!mft) {
        free(parent);
        printf("FILE NOT FOUND\n");
        return;
    }

    if (mft->is_directory) {
        free(parent);
        printf("FILE NOT FOUND\n");
        return;
    }

    content = malloc(mft->item_size);
    if (!content) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }

    read_content(content, mft, runtime);
    dest_file = fopen(command->args[1], "w");
    if (!dest_file) {
        printf("PATH NOT FOUND\n");
        exit(1);
    }
    fwrite(content, mft->item_size, 1, dest_file);
    fclose(dest_file);

    free(parent);
    free(content);
    printf("OK\n");
}

/**
 * Handle cat command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_cat_command(struct command_t * command, struct runtime_t * runtime) {
    int i;
    struct directory_descriptor * parent;
    struct mft_item * mft = NULL;
    void * content;

    parent = malloc(sizeof(struct directory_descriptor));
    read_content(parent, runtime->mfts[runtime->pwd_mft_index], runtime);

    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (parent->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[parent->files_mft[i]]->item_name, command->args[0])) {
            mft = runtime->mfts[parent->files_mft[i]];
            break;
        }
    }

    if (!mft) {
        free(parent);
        printf("FILE NOT FOUND\n");
        return;
    }

    if (mft->is_directory) {
        free(parent);
        printf("FILE NOT FOUND\n");
        return;
    }

    content = malloc(mft->item_size);
    if (!content) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }

    read_content(content, mft, runtime);
    fwrite(content, mft->item_size, 1, stdout);
    printf("\n");
    free(parent);
    free(content);
}

/**
 * Handle load command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_load_command(struct command_t * command, struct runtime_t * runtime) {
    struct command_t * batch_command;
    char line_buffer[256];
    int i, c, done;
    char * command_str;
    FILE * batch_file;

    batch_file = fopen(command->args[0], "r");
    if (!batch_file) {
        printf("FILE NOT FOUND\n");
        return;
    }

    i = 0;
    done = 0;
    while (!done) {
        c = fgetc(batch_file);
        if (c == EOF) {
            c = '\n';
            done = 1;
        }
        line_buffer[i++] = (char) c;
        if (c == '\n') {
            line_buffer[i-1] = '\0';
            command_str = malloc(i);
            if (!command_str) {
                printf("Cannot allocate memory.\n");
                exit(1);
            } 
            strcpy(command_str, line_buffer);
            batch_command = parse_command(command_str);
            if (batch_command) {
                handle_command(batch_command, runtime);
                free(batch_command);
            }
			free(command_str);
            i = 0;
        }
    }

    fclose(batch_file);
    printf("OK\n");
}

/**
 * Handle cp command
 * @param command parsed command
 * @param runtime runtime instance
 */
int handle_cp_command(struct command_t * command, struct runtime_t * runtime) {
    char * token, ** tokens;
    int token_index, i, j, copy_file_mft_index, to_save, fragment_index, free_clusters_count, dir_found, start_addr;
    struct mft_item * source_mft, * current_mft, * target_mft, * original_file_mft, * copy_file_mft;
    struct directory_descriptor * dir;
    void * file_content;
    copy_file_mft = NULL;
    copy_file_mft_index = 0;

    //prectu source sekvenci umisteni
    token_index = 0;
    token = strtok(command->args[0], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    //do source_mft ulozim mft_item zdrojove slozky
    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        dir_found = 0;
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                dir_found = 1;
                break;
            }
        }
        free(dir);
        if (!dir_found) {
            printf("PATH NOT FOUND\n");
            return 0;
        }
    }

    source_mft = current_mft;
    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, source_mft, runtime);
    original_file_mft = NULL;
    for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
        if (dir->files_mft[j] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
            file_content = malloc(runtime->mfts[dir->files_mft[j]]->item_size);
            if (!file_content) {
                printf("Cannot allocate memory.\n");
                exit(1);
            }
            original_file_mft = runtime->mfts[dir->files_mft[j]];
            read_content(file_content, runtime->mfts[dir->files_mft[j]], runtime);
            break;
        }
    }

    if (!original_file_mft) {
        printf("FILE NOT FOUND\n");
        return 0;
    }

    token_index = 0;
    token = strtok(command->args[1], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        dir_found = 0;
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                dir_found = 1;
                break;
            }
        }
        if (!dir_found) {
            printf("PATH NOT FOUND\n");
            return 0;
        }
    }
    target_mft = current_mft;

    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, target_mft, runtime);
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (dir->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[dir->files_mft[i]]->item_name, tokens[token_index-1])) {
            printf("Target file already exists.\n");
            return 0;
        }
    }

    //find free mft
    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        if (runtime->mfts[i]->uid == UID_ITEM_FREE) {
            copy_file_mft = runtime->mfts[i];
            copy_file_mft_index = i;
            break;
        }
    }

    if (!copy_file_mft) {
        printf("Free MFT not found.\n");
        return 0;
    }

    copy_file_mft->uid = copy_file_mft_index;
    copy_file_mft->item_size = original_file_mft->item_size;
    copy_file_mft->item_order = 1;
    copy_file_mft->item_order_total = 1;
    copy_file_mft->is_directory = 0;
    for (i = 0; i < FILENAME_MAX; i++) {
        copy_file_mft->item_name[i] = tokens[token_index-1][i];
        if (copy_file_mft->item_name[i] == '\0') {
            break;
        }
    }

    to_save = copy_file_mft->item_size;
    fragment_index = 0;
    while (to_save > 0) {
        copy_file_mft->item_order_total++;
        for (i = 0; i < runtime->br->cluster_count; i++) {
            if (!runtime->bitmap[i]) {
                free_clusters_count = 1;
                start_addr = i;
                while (!runtime->bitmap[++i]) {
                    free_clusters_count++;
                }
                if (free_clusters_count > ((to_save / DATA_CLUSTER_SIZE) + (to_save % DATA_CLUSTER_SIZE > 0) ? 1 : 0)) {
                    free_clusters_count = ((to_save / DATA_CLUSTER_SIZE) + (to_save % DATA_CLUSTER_SIZE > 0) ? 1 : 0);
                }
                for (j = 0; j < free_clusters_count; j++) {
                    runtime->bitmap[start_addr + j] = 1;
                }
                copy_file_mft->fragments[fragment_index].fragment_count = free_clusters_count;
                copy_file_mft->fragments[fragment_index].fragment_start_address = start_addr;

                fseek(runtime->file, runtime->data_start, SEEK_SET);
                fseek(runtime->file, start_addr * DATA_CLUSTER_SIZE, SEEK_CUR);

                fwrite(file_content, free_clusters_count * DATA_CLUSTER_SIZE > to_save ? to_save : free_clusters_count * DATA_CLUSTER_SIZE, 1, runtime->file);
                file_content += free_clusters_count * DATA_CLUSTER_SIZE > to_save ? to_save : free_clusters_count * DATA_CLUSTER_SIZE;
                to_save -= free_clusters_count * DATA_CLUSTER_SIZE > to_save ? to_save : free_clusters_count * DATA_CLUSTER_SIZE;

                break;
            }
        }

        fragment_index++;
    }

    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, target_mft, runtime);
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (dir->files_mft[i] == DIRECTORY_FILE_FREE) {
            dir->files_mft[i] = copy_file_mft_index;
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, target_mft->fragments[0].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            fwrite(dir, sizeof(struct directory_descriptor), 1, runtime->file);
            break;
        }
    }

    persist_vfs(runtime);
    printf("OK\n");
    return 1;
}

/**
 * Handle rm command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_rm_command(struct command_t * command, struct runtime_t * runtime) {
    char * token, ** tokens;
    int token_index, i, j, to_remove;
    struct mft_item * current_mft, * file_mft;
    struct directory_descriptor * dir;

    //prectu source sekvenci umisteni
    token_index = 0;
    token = strtok(command->args[0], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                break;
            }
        }
        free(dir);
    }

    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, current_mft, runtime);

    file_mft = NULL;
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (dir->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[dir->files_mft[i]]->item_name, tokens[token_index - 1])) {
            file_mft = runtime->mfts[dir->files_mft[i]];
            dir->files_mft[i] = DIRECTORY_FILE_FREE;
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, current_mft->fragments[0].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            fwrite(dir, sizeof(struct directory_descriptor), 1, runtime->file);
            break;
        }
    }

    if (!file_mft) {
        printf("FILE NOT FOUND\n");
        return;
    }

    file_mft->uid = UID_ITEM_FREE;
    to_remove = file_mft->item_size;

    for (i = 0; i < MFT_FRAGMENTS_COUNT; i++) {
        for (j = 0; j < file_mft->fragments[0].fragment_count; j++) {
            runtime->bitmap[file_mft->fragments[i].fragment_start_address + j] = 0;
            to_remove -= DATA_CLUSTER_SIZE;
        }
        if (to_remove <= 0) {
            break;
        }
    }

    persist_vfs(runtime);
    printf("OK\n");
}

/**
 * Handle mv command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_mv_command(struct command_t * command, struct runtime_t * runtime) {
    struct command_t * rem_cmd = malloc(sizeof(struct command_t));
    if (!rem_cmd) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    rem_cmd->type = COMMAND_TYPE_RM;
    rem_cmd->argc = COMMAND_ARGC_RM;
    rem_cmd->args = malloc(sizeof(char *));
    if (!rem_cmd->args) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    rem_cmd->args[0] = malloc(sizeof(char) * (strlen(command->args[0]) + 1));
    if (!rem_cmd->args[0]) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    strcpy(rem_cmd->args[0], command->args[0]);

    if (handle_cp_command(command, runtime)) {
        handle_rm_command(rem_cmd, runtime);
    }

    free(rem_cmd);
    persist_vfs(runtime);
}

/**
 * Handle info command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_info_command(struct command_t * command, struct runtime_t * runtime) {
    char * token, ** tokens;
    int token_index, i, j, to_read;
    struct mft_item * current_mft, * file_mft;
    struct directory_descriptor * dir;

    token_index = 0;
    token = strtok(command->args[0], "/");
    tokens = malloc(sizeof(char *));
    if (!tokens) {
        printf("Cannot allocate memory.\n");
        exit(1);
    }
    tokens[token_index++] = token;
    while ((token = strtok(NULL, "/")) != NULL) {
        tokens = realloc(tokens, (token_index + 1) * sizeof(char *));
        tokens[token_index++] = token;
    }

    current_mft = runtime->mfts[runtime->pwd_mft_index];
    for (i = 0; i < (token_index - 1); i++) {
        dir = malloc(sizeof(struct directory_descriptor));
        read_content(dir, current_mft, runtime);
        for (j = 0; j < MAX_DIRECTORY_FILES_COUNT; j++) {
            if (dir->files_mft[j] != DIRECTORY_FILE_FREE && runtime->mfts[dir->files_mft[j]]->is_directory && !strcmp(runtime->mfts[dir->files_mft[j]]->item_name, tokens[i])) {
                current_mft = runtime->mfts[dir->files_mft[j]];
                break;
            }
        }
        free(dir);
    }

    file_mft = NULL;
    dir = malloc(sizeof(struct directory_descriptor));
    read_content(dir, current_mft, runtime);
    for (i = 0; i < MAX_DIRECTORY_FILES_COUNT; i++) {
        if (dir->files_mft[i] != DIRECTORY_FILE_FREE && !strcmp(runtime->mfts[dir->files_mft[i]]->item_name, tokens[token_index - 1])) {
            file_mft = runtime->mfts[dir->files_mft[i]];
            break;
        }
    }
    free(dir);

    if (!file_mft) {
        printf("FILE NOT FOUND\n");
        return;
    }

    printf("Name: %s\n", file_mft->item_name);
    printf("UID: %d\n", file_mft->uid);

    to_read = file_mft->item_size;
    for (i = 0; i < MFT_FRAGMENTS_COUNT; i++) {
        printf("Fragment %d - Clusters %d to %d \n", i, file_mft->fragments[i].fragment_start_address, file_mft->fragments[i].fragment_start_address + file_mft->fragments[i].fragment_count - 1);
        to_read -= file_mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE;
        if (to_read <= 0) {
            break;
        }
    }
    printf("OK\n");
}

/**
 * Handle defrag command
 * @param command parsed command
 * @param runtime runtime instance
 */
void handle_defrag_command(struct runtime_t * runtime) {
    int used_clusters, i, j, k, current_pos, to_move;
    size_t reading;
    void * data_buffer;

    used_clusters = 0;
    for (i = 0; i < runtime->br->cluster_count; i++) {
        if (runtime->bitmap[i]) {
            used_clusters++;
        }
    }

    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        if (runtime->mfts[i]->uid == UID_ITEM_FREE) {
            continue;
        }
        to_move = runtime->mfts[i]->item_size;
        for (j = 0; j < MFT_FRAGMENTS_COUNT; j++) {
            data_buffer = malloc((size_t)runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE);
            if (!data_buffer) {
                printf("Cannot allocate memory.");
                exit(1);
            }
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, runtime->mfts[i]->fragments[j].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            reading = runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE > to_move ? (size_t) to_move : runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE;
            fread(data_buffer, reading, 1, runtime->file);

            for (k = 0; k < runtime->mfts[i]->fragments[j].fragment_count; k++) {
                runtime->bitmap[runtime->mfts[i]->fragments[j].fragment_start_address + k] = 0;
            }

            runtime->mfts[i]->fragments[j].fragment_start_address += used_clusters;
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, runtime->mfts[i]->fragments[j].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            fwrite(data_buffer, reading, 1, runtime->file);
            free(data_buffer);

            for (k = 0; k < runtime->mfts[i]->fragments[j].fragment_count; k++) {
                runtime->bitmap[runtime->mfts[i]->fragments[j].fragment_start_address + k] = 1;
            }

            to_move -= reading;
            if (to_move <= 0 ) {
                break;
            }
        }
    }

    current_pos = 0;
    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        if (runtime->mfts[i]->uid == UID_ITEM_FREE) {
            continue;
        }
        to_move = runtime->mfts[i]->item_size;
        for (j = 0; j < MFT_FRAGMENTS_COUNT; j++) {
            data_buffer = malloc((size_t)runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE);
            if (!data_buffer) {
                printf("Cannot allocate memory.");
                exit(1);
            }
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, runtime->mfts[i]->fragments[j].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            reading = runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE > to_move ? (size_t) to_move : runtime->mfts[i]->fragments[j].fragment_count * DATA_CLUSTER_SIZE;
            fread(data_buffer, reading, 1, runtime->file);

            for (k = 0; k < runtime->mfts[i]->fragments[j].fragment_count; k++) {
                runtime->bitmap[runtime->mfts[i]->fragments[j].fragment_start_address + k] = 0;
            }

            runtime->mfts[i]->fragments[j].fragment_start_address = current_pos;
            current_pos += runtime->mfts[i]->fragments[j].fragment_count;
            fseek(runtime->file, runtime->data_start, SEEK_SET);
            fseek(runtime->file, runtime->mfts[i]->fragments[j].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
            fwrite(data_buffer, reading, 1, runtime->file);
            free(data_buffer);

            for (k = 0; k < runtime->mfts[i]->fragments[j].fragment_count; k++) {
                runtime->bitmap[runtime->mfts[i]->fragments[j].fragment_start_address + k] = 1;
            }

            to_move -= reading;
            if (to_move <= 0 ) {
                break;
            }
        }
    }

    persist_vfs(runtime);
    printf("OK\n");
}

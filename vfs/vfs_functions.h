#include "../structures/boot_record.h"
#include "../structures/mft_item.h"
#include "../runtime/runtime.h"

#ifndef PSEUDONTFS_VFS_FUNCTIONS_H
#define PSEUDONTFS_VFS_FUNCTIONS_H

/**
 * Persists filesystem to file
 * @param runtime runtime instance
 */
void persist_vfs(struct runtime_t * runtime);

/**
 * Loads filesystem from file
 * @param runtime runtime instance
 */
void load_vfs(struct runtime_t * runtime);

/**
 * Read content from filesystem file
 * @param dest destination pointer
 * @param mft MFT item
 * @param runtime runtime instance
 */
void read_content(void * dest, struct mft_item * mft, struct runtime_t * runtime);

#endif //PSEUDONTFS_VFS_FUNCTIONS_H

#include <stdlib.h>
#include "vfs_functions.h"
#include "../structures/boot_record.h"
#include "../structures/mft_fragment.h"
#include "../structures/mft_item.h"
#include "../structures/directory_descriptor.h"

/**
 * Persists filesystem to file
 * @param runtime runtime instance
 */
void persist_vfs(struct runtime_t * runtime) {
    int i;
    fseek(runtime->file, 0, SEEK_SET);
    fwrite(runtime->br, sizeof(struct boot_record), 1, runtime->file);
    for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
        fwrite(runtime->mfts[i], sizeof(struct mft_item), 1, runtime->file);
    }
    fwrite(runtime->bitmap, sizeof(int32_t), (size_t) runtime->br->cluster_count, runtime->file);
    runtime->data_start = ftell(runtime->file);
}

/**
 * Loads filesystem from file
 * @param runtime runtime instance
 */
void load_vfs(struct runtime_t * runtime) {
    int i;
    fseek(runtime->file, 0, SEEK_SET);
    if (!runtime->br) {
        runtime->br = malloc(sizeof(struct boot_record));
    }

    fread(runtime->br, sizeof(struct boot_record), 1, runtime->file);
    if (!runtime->mfts) {
        runtime->mfts = malloc(sizeof(struct mft_item *) * runtime->br->mft_max_fragment_count);
        for (i = 0; i < runtime->br->mft_max_fragment_count; i++) {
            runtime->mfts[i] = malloc(sizeof(struct mft_item));
            fread(runtime->mfts[i], sizeof(struct mft_item), 1, runtime->file);
        }

    }
    if (!runtime->bitmap) {
        runtime->bitmap = malloc(sizeof(int32_t) * runtime->br->cluster_count);
    }
    fread(runtime->bitmap, sizeof(int32_t), (size_t) runtime->br->cluster_count, runtime->file);
    runtime->data_start = ftell(runtime->file);
}

/**
 * Read content from filesystem file
 * @param dest destination pointer
 * @param mft MFT item
 * @param runtime runtime instance
 */
void read_content(void * dest, struct mft_item * mft, struct runtime_t * runtime) {
    int i, to_read;
    to_read = mft->item_size;
    for (i = 0; i < mft->item_order_total; i++) {
        fseek(runtime->file, sizeof(struct boot_record) + (sizeof(struct mft_item) * (size_t) runtime->br->mft_max_fragment_count) + (sizeof(int32_t) * runtime->br->cluster_count), SEEK_SET);
        fseek(runtime->file, mft->fragments[i].fragment_start_address * DATA_CLUSTER_SIZE, SEEK_CUR);
        fread(dest, mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE > to_read ? to_read : mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE, 1, runtime->file);
        to_read -= mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE > to_read ? to_read : mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE;
        dest += mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE > to_read ? to_read : mft->fragments[i].fragment_count * DATA_CLUSTER_SIZE;
    }
}
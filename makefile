CC=gcc

all: dirs build clean

.PHONY: commands_handler
.PHONY: commands_parser
.PHONY: logger
.PHONY: runtime
.PHONY: vfs_functions

dirs:
	mkdir -p bin

build: commands logger runtime vfs
	${CC} -o bin/vfs main.c bin/commands_handler.o bin/commands_parser.o bin/logger.o bin/runtime.o bin/vfs_functions.o

commands: commands_handler commands_parser

commands_handler: commands/commands_handler.c commands/commands_handler.h
	${CC} -c -o bin/commands_handler.o commands/commands_handler.c

commands_parser: commands/commands_parser.c commands/commands_parser.h
	${CC} -c -o bin/commands_parser.o commands/commands_parser.c

logger: logger/logger.c logger/logger.h
	${CC} -c -o bin/logger.o logger/logger.c

runtime: runtime/runtime.c runtime/runtime.h
	${CC} -c -o bin/runtime.o runtime/runtime.c

vfs: vfs_functions

vfs_functions: vfs/vfs_functions.c vfs/vfs_functions.h
	${CC} -c -o bin/vfs_functions.o vfs/vfs_functions.c

clean:
	rm -f bin/*.o

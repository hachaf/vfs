#include <stdio.h>
#include "runtime/runtime.h"

int main(int argc, char * argv[]) {
    if (!check_args(argc, argv)) {
        return 0;
    }
    return run_application(argv[1]);
}
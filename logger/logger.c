#include "logger.h"
#include <time.h>
#include <stdio.h>

void _log_message(const char * str, const char * level);

/**
 * Logs info message
 * @param str message
 */
void log_info(const char * str) {
    _log_message(str, "INFO");
}

/**
 * Logs error message
 * @param str message
 */
void log_error(const char * str) {
    _log_message(str, "ERROR");
}

/**
 * Logs message
 * @param str message
 * @param level log level
 */
void _log_message(const char * str, const char * level) {
    time_t raw_time;
    struct tm * time_info;
    time(&raw_time);
    time_info = localtime(&raw_time);
    printf("[%s][%d-%d-%d %d:%d:%d] %s\n", level, time_info->tm_year + 1900, time_info->tm_mon + 1, time_info->tm_mday, time_info->tm_hour, time_info->tm_min, time_info->tm_sec, str);
}
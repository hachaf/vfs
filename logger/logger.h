#ifndef PSEUDONTFS_LOGGER_H
#define PSEUDONTFS_LOGGER_H

/**
 * Logs info message
 * @param str message
 */
void log_info(const char * str);

/**
 * Logs error message
 * @param str message
 */
void log_error(const char * str);

#endif //PSEUDONTFS_LOGGER_H

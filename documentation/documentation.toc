\babel@toc {czech}{}
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{2}{section.1}
\contentsline {section}{\numberline {2}Zad\IeC {\'a}n\IeC {\'\i }}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}T\IeC {\'e}ma pr\IeC {\'a}ce}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Informace k zad\IeC {\'a}n\IeC {\'\i } a omezen\IeC {\'\i }m}{5}{subsection.2.2}
\contentsline {section}{\numberline {3}Popis implementace}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Jazyk a prost\IeC {\v r}ed\IeC {\'\i }}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Datov\IeC {\'e} struktury}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Spou\IeC {\v s}t\IeC {\v e}c\IeC {\'\i } z\IeC {\'a}znam}{6}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}MFT z\IeC {\'a}znam}{6}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}MFT fragment}{6}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Adres\IeC {\'a}\IeC {\v r}}{6}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}\IeC {\v C}ten\IeC {\'\i } a z\IeC {\'a}pis dat}{7}{subsection.3.3}
\contentsline {section}{\numberline {4}U\IeC {\v z}ivatelsk\IeC {\'a} p\IeC {\v r}\IeC {\'\i }ru\IeC {\v c}ka}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}P\IeC {\v r}eklad aplikace}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } aplikace}{7}{subsection.4.2}
\contentsline {section}{\numberline {5}Z\IeC {\'a}v\IeC {\v e}r}{7}{section.5}

#ifndef PSEUDONTFS_RUNTIME_H
#define PSEUDONTFS_RUNTIME_H

#include <stdio.h>
#include "../structures/directory_descriptor.h"
#include "../structures/boot_record.h"
#include "../structures/mft_item.h"

struct runtime_t {
    char * filepath;
    int filesystem_exists;
    FILE * file;
    struct boot_record * br;
    struct mft_item ** mfts;
    int32_t * bitmap;
    int32_t pwd_mft_index;
    int32_t exit;
    int32_t data_start;
};

/**
 * Checks given arguments
 * @param argc coutn of arguments
 * @param argv arguments
 * @return T/F
 */
int check_args(int argc, char * argv[]);

/**
 * Main runtime loop
 * @param filepath filesystem filepath
 * @return exit code
 */
int run_application(char * filepath);

/**
 * Creates runtime instance
 * @param filepath filesystem filepath
 * @return runtime instance
 */
struct runtime_t * initiate_runtime(char * filepath);

#endif //PSEUDONTFS_RUNTIME_H

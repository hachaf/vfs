#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "runtime.h"
#include "../logger/logger.h"
#include "../commands/commands.h"
#include "../commands/commands_parser.h"
#include "../commands/commands_handler.h"
#include "../vfs/vfs_functions.h"

/**
 * Checks given arguments
 * @param argc coutn of arguments
 * @param argv arguments
 * @return T/F
 */
int check_args(int argc, char * argv[]) {
    if (argc < 2) {
        log_error("Filepath argument missing.");
        return 0;
    }
    if (!argv[1] || strlen(argv[1]) == 0) {
        log_error("Filepath argument cannot be empty.");
        return 0;
    }
    return 1;
}

/**
 * Main runtime loop
 * @param filepath filesystem filepath
 * @return exit code
 */
int run_application(char * filepath) {
    struct runtime_t * runtime_instance = initiate_runtime(filepath);

    while (!runtime_instance->exit) {
        struct command_t * command = read_command();
        if (command) {
            handle_command(command, runtime_instance);
        }
    }

    free(runtime_instance);
    return 0;
}

/**
 * Creates runtime instance
 * @param filepath filesystem filepath
 * @return runtime instance
 */
struct runtime_t * initiate_runtime(char * filepath) {
    struct runtime_t * result = malloc(sizeof(struct runtime_t));
    if (!result) {
        log_error("Cannot allocate memory.");
        exit(0);
    }
    result->filepath = filepath;
    result->pwd_mft_index = 0;
    result->br = NULL;
    result->mfts = NULL;
    result->bitmap = NULL;
    result->exit = 0;
    FILE * f = fopen(filepath, "r");
    if (!f) {
        result->filesystem_exists = 0;
        log_info("Filesystem does not exist. Format new filesystem with \"format <size in MB>\" command.");
    } else {
        result->filesystem_exists = 1;
        result->file = fopen(result->filepath, "r+");
        log_info("Filesystem file found.");
        load_vfs(result);
        log_info("Filesystem loaded.");
    }
    return result;
}
#ifndef PSEUDONTFS_BOOT_RECORD_H
#define PSEUDONTFS_BOOT_RECORD_H

#include <sys/types.h>
#include "directory_descriptor.h"

#define BOOT_RECORD_SIZE 288

#define  UID_ITEM_FREE -1

struct boot_record {
    char signature[9];              //login autora FS
    char volume_descriptor[251];    //popis vygenerovaného FS
    int32_t disk_size;              //celkova velikost VFS
    int32_t cluster_size;           //velikost clusteru
    int32_t cluster_count;          //pocet clusteru
    int32_t mft_start_address;      //adresa pocatku mft
    int32_t bitmap_start_address;   //adresa pocatku bitmapy
    int32_t data_start_address;     //adresa pocatku datovych bloku
    int32_t mft_max_fragment_count; //maximalni pocet fragmentu v jednom zaznamu v mft (pozor, ne souboru)
    int32_t root_dir_mft_index;
    // stejne jako   MFT_FRAGMENTS_COUNT
};

#endif //PSEUDONTFS_BOOT_RECORD_H
#ifndef PSEUDONTFS_MFT_FRAGMENT_H
#define PSEUDONTFS_MFT_FRAGMENT_H

#include <sys/types.h>

#define DATA_CLUSTER_SIZE (32 * 1024)

struct mft_fragment {
    int32_t fragment_start_address;     //start adresa
    int32_t fragment_count;             //pocet clusteru ve fragmentu
};

#endif //PSEUDONTFS_MFT_FRAGMENT_H
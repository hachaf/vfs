#ifndef PSEUDONTFS_DIRECTORY_DESCRIPTOR_H
#define PSEUDONTFS_DIRECTORY_DESCRIPTOR_H

#include <sys/types.h>

#define MAX_DIRECTORY_FILES_COUNT 64
#define DIRECTORY_FILE_FREE -1

struct directory_descriptor {
    int32_t files_mft[MAX_DIRECTORY_FILES_COUNT];
    int32_t parent_mft;
    int32_t is_root;
};

#endif //PSEUDONTFS_DIRECTORY_DESCRIPTOR_H
